﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UI_HideForInterfaceForScreenshot : MonoBehaviour
{
    public UnityEvent m_onHide;
    public UnityEvent m_onUnhide;
    public bool m_hideStateDebug ;

    public void HideFor(float seconds) {

        Hide(true);
        Invoke("Unhide", seconds);
    }

    public void Hide()
    {

        Hide(true);
    }
    public void Unhide()
    {

        Hide(false);
    }
    public void Hide(bool setAsHide)
    {
        m_hideStateDebug = setAsHide;

        if (m_hideStateDebug)
        {
            m_onHide.Invoke();
        }
        else
        {
            m_onUnhide.Invoke();
        }
    }
}
