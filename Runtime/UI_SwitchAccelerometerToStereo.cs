﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UI_SwitchAccelerometerToStereo : MonoBehaviour
{
    public UnityEvent m_activateStereo;
    public UnityEvent m_deactivateStereo;
    public UnityEvent m_activateAccelerometer;
    public UnityEvent m_deactivateAccelerometer;
    public bool m_focusOnAccelerometer=true;

    public void Start()
    {
        SwitchMode(true);
    }

    public void SwitchMode()
    {

        SwitchMode(!m_focusOnAccelerometer);
    }
        public void SwitchMode(bool focusAccelerometer) {
        m_focusOnAccelerometer = focusAccelerometer;

        if (m_focusOnAccelerometer)
        {
            m_activateAccelerometer.Invoke();
            m_deactivateStereo.Invoke();
        }
        else
        {
            m_activateStereo.Invoke();
            m_deactivateAccelerometer.Invoke();
        }
    }
}
